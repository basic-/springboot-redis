package com.redis.controller;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

/**
 *使用spring cache ，注解方式操作redis
 * 依赖启动配置 @EnableCaching//开启缓存,使用注解操作redis
 * 依赖pom配置  <artifactId>spring-boot-starter-cache</artifactId>
 */
@RestController
@RequestMapping("/spring-cache")
public class SpringCacheController {

    @GetMapping("/data/{id}")
    @Cacheable(value = "dataCache", key = "#id")
    public String getData(@PathVariable String id) {
        // 从数据库或其他数据源中获取数据
        // 返回数据
        String data = "Test Data";
        return data;
    }

    @PostMapping("/data/{id}")
    @CachePut(value = "dataCache", key = "#id")
    public String updateData(@PathVariable String id) {
        // 更新数据库或其他数据源中的数据
        // 返回更新后的数据
        String updatedData = "Updated Test Data";
        return updatedData;
    }

    @DeleteMapping("/data/{id}")
    @CacheEvict(value = "dataCache", key = "#id")
    public void deleteData(@PathVariable String id) {
        // 从数据库或其他数据源中删除数据
    }
}
