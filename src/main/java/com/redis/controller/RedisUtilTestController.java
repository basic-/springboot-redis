package com.redis.controller;

import com.redis.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *使用RedisUtil，操作redis。依赖RedisConfig，RedisUtil
 */
@RestController
public class RedisUtilTestController {
    @Autowired
    private RedisUtil redisUtil;

    @PostMapping("/set1")
    public ResponseEntity<String> set1(@RequestParam String key, @RequestParam String value) {
        // 处理POST请求逻辑，将参数存储到Redis中
        redisUtil.set(key,value);
        return ResponseEntity.ok("OK");
    }

    @GetMapping("/get1/{key}")
    public ResponseEntity<String> get1(@PathVariable String key) {
        // 处理GET请求逻辑，从Redis中获取指定键的值
        String value = (String) redisUtil.get(key);
        if (value != null) {
            return ResponseEntity.ok(value);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
