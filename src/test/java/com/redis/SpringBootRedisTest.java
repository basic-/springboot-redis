/**
 * @author CH_ywx
 * @Date 2023-05-31
 * @Description
 */
package com.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 *@author CH_ywx
 *@Date 2023-05-31
 *@Description 整合Redis操作测试类
 */
@SpringBootTest
public class SpringBootRedisTest {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void fun1(){
        //存
        redisTemplate.opsForValue().set("name","lucy");
        //取
        String name = redisTemplate.opsForValue().get("name");
        System.out.println(name);
    }
}
