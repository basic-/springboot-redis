

本教程将指导您如何在 Spring Boot  项目中整合 Redis，实现常见的缓存操作。

## 步骤

以下是整合 Redis 的步骤：

### 1. 引入 Redis 依赖

在项目的 `pom.xml` 文件中添加 Redis 依赖：

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

### 2. 配置 Redis 连接信息

在项目的 `application.yml`（或 `application.properties`）文件中配置 Redis 连接信息：

```yaml
server:
  port: 8083
# REDIS
# Redis数据库索引（默认为0）
spring:
  redis:
    host: 192.168.206.132
    port: 6379
    password: 1234
    lettuce:
      pool:
        # 连接池最大连接数（使用负值表示没有限制） 默认 8
        max-active: 8
        # 连接池最大阻塞等待时间（使用负值表示没有限制） 默认 -1
        max-wait: -1
        # 连接池中的最大空闲连接 默认 8
        max-idle: 8
        # 连接池中的最小空闲连接 默认 0
        min-idle: 0
  # 开启缓存,使用注解操作redis
  cache:
    type: redis

```

### 3. 创建 Redis 配置类

创建一个 `RedisConfig` 类，用于配置 RedisTemplate ：

```java
@Configuration
public class RedisConfig {

    @Bean
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(factory);
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }

}
```

### 4. 使用 Redis

现在，您可以在您的 Spring Boot 应用程序中使用 Redis 了。例如，以下是一个示例：

```java
/**
 *使用redisTemplate，操作redis
 */
@RestController
public class RedisTemplateTestController {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping("/set")
    public ResponseEntity<String> set(@RequestParam String key, @RequestParam String value) {
        // 处理POST请求逻辑，将参数存储到Redis中
            redisTemplate.opsForValue().set(key, value);
        return ResponseEntity.ok("OK");
    }

    @GetMapping("/get/{key}")
    public ResponseEntity<String> get(@PathVariable String key) {
        // 处理GET请求逻辑，从Redis中获取指定键的值
        String value = redisTemplate.opsForValue().get(key);
        if (value != null) {
            return ResponseEntity.ok(value);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete/{key}")
    public ResponseEntity<String> delete(@PathVariable String key) {
        // 处理DELETE请求逻辑，删除Redis中指定的键值对
        Boolean deleted = redisTemplate.delete(key);
        if (deleted) {
            return ResponseEntity.ok("Deleted successfully");
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
```
